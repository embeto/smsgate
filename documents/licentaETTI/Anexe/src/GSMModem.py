from Errors import *
from SerialCOM import *

from time import sleep
from string import replace
import RPi.GPIO as GPIO
import threading
import traceback

class GSMModem:
    status = False
    noSMS = 0
    totSMS = 0

    def __init__(self):
        self.lock = threading.Lock()

    def powerOn(self):
        self.refreshStat(True)
        if self.status:
            raise ModemAlreadyOnError
        else:
            try:
                GPIO.output(POWER, GPIO.LOW)
                sleep(0.1)
                GPIO.output(POWER, GPIO.HIGH)
                sleep(0.4)
                GPIO.output(POWER, GPIO.LOW)
                sleep(0.1)
            except:
                raise GPIOoutputError
        sleep(5)
        self.refreshStat(True)
        if not self.status:
            raise ModemPowerOnError


    def powerOff(self):
        self.refreshStat(True)
        if not self.status:
            raise ModemAlreadyOffError
        else:
            try:
                serialCOM.sendATCommand("AT+QPOWD", ["OK", "ERROR"], 1)
            except:
                self.status=True
                return
            else:
                self.status=False
        sleep(5)
        self.refreshStat(True)
        if self.status:
            raise ModemPowerOffError


    def restart(self):
        self.powerOff()
        sleep(1)
        self.powerOn()


    def reset(self):
        try:
            GPIO.output(RESET, GPIO.LOW)
            sleep(1)
            GPIO.output(RESET, GPIO.HIGH)
            sleep(5)
        except:
            raise GPIOoutputError


    def refreshStat(self, mode):
        if mode:
            try:
                self.status = GPIO.input(STATUS)
            except:
                self.status = False
                raise GPIOinputError
        else:
            try:
                serialCOM.connection.write("+++\r\n")
                serialCOM.connection.write(chr(0x1B) + "\r\n")
                serialCOM.connection.write("AT\r\n")
                serialCOM.connection.write("ATE1\r\n")
                serialCOM.connection.write("ATV1\r\n")
                serialCOM.clearSInput()
                serialCOM.clearInput()
            except:
                raise SerialSetupError
            try:
                serialCOM.sendATCommand("AT", ["OK", "ERROR"], 1)
            except:
                self.status=False
                return
            else:
                self.status=True


    def setup(self):
        try:
            serialCOM.sendATCommand("AT+QIMODE=0", ["OK", "ERROR"], 3)                            # Select TCPIP transferring mode
            serialCOM.sendATCommand("AT+QINDI=0", ["OK", "ERROR"], 3)                             # Set the method to handle TCPIP data
            serialCOM.sendATCommand("AT+QIMUX=0", ["OK", "ERROR"], 3)                             # Control whether to enable TCPIP data
            serialCOM.sendATCommand("AT+QIDNSIP=0", ["OK", "ERROR"], 3)                           # Connect with IP address or DNS
            serialCOM.sendATCommand("AT+CSCS=\"IRA\"",["OK","ERROR"],3)                           # Select TE character set to Intern Reference Alphabet
            serialCOM.sendATCommand("AT+CPMS=\"SM\",\"SM\",\"SM\"", ["OK", "+CMS ERROR:"], 3)     #Select storage for SMS mem1 = SM, mem2 = SM, mem3 = SM
        except:
            raise GSMModuleSetupError

        while True:
            serialCOM.clearInput()
            try:
                serialCOM.sendATCommand("AT+CPIN?", ["OK", "ERROR"], 3)
            except:
                traceback.print_exc()
                raise GSMModuleSetupError
            else:
                if (serialCOM.dataBuffer.find("READY")):
                    break
                else:
                    raise RequestedPinError
        sleep(1)
        while True:
            serialCOM.clearInput()
            try:
                serialCOM.sendATCommand("AT+CPBS?", ["OK", "ERROR"], 3)                           # select phonebook memory storage
            except:
                raise GSMModuleSetupError
            if serialCOM.dataBuffer.find("+CME ERROR") < 0:
                break
            sleep(0.5)

        try:
            serialCOM.sendATCommand("AT+CPBS=\"SM\"", ["OK", "ERROR"], 5)                         # set phonebook memory storage as SIM
            serialCOM.sendATCommand("AT+CMGF=1", ["OK", "ERROR"], 3)                              # select text mode format
            serialCOM.sendATCommand("AT+CNMI=2,0,0,0,0\r", ["OK", "ERROR"], 3)                    # SMS event reporting configuration
        except:
            raise GSMModuleSetupError
        serialCOM.clearInput()


    def getIMEI(self):
        try:
            serialCOM.sendATCommand("AT+GSN", ["OK", "ERROR"], 3)
        except:
            raise IMEIQuerryError
        IMEI = serialCOM.dataBuffer[9:-8]
        return IMEI


    def getIMSI(self):
        serialCOM.clearInput()
        try:
            serialCOM.sendATCommand("AT+CIMI", ["OK", "ERROR"], 3)
        except:
            raise IMSIQuerryError
        else:
            if len(serialCOM.dataBuffer) > 18:
                return dataBuffer[dataBuffer.find("AT+CIMI") + 10:dataBuffer.find("OK") - 4]
            else:
                raise IMSIQuerryError


    def checkNwReg(self, timeout):
        startTime = time.time()
        while (time.time() - startTime < timeout):
            try:
                serialCOM.sendATCommand("AT+CREG?", ["OK", "ERROR"], 10)
            except:
                raise NwRegistrationError
            else:
                if serialCOM.dataBuffer.find("0,1") > 0 or serialCOM.dataBuffer.find("0,5") > 0:
                    return
                else:
                    pass
                sleep(0.5)
        raise NwRegistrationError


    def getSignalStat(self):
        level = 0
        try:
            serialCOM.sendATCommand("AT+CSQ", ["OK", "ERROR"], 10)
        except:
            raise SignalLevelQuerryError
        else:
            dataBuffer = serialCOM.dataBuffer
            startat = dataBuffer.find(": ")
            endat = dataBuffer.find(",")
            result = int(dataBuffer[startat + 2:endat])
            if result == 99:
                level = 0
            elif result > -1 and result < 8:        # -113dBm -> -99dBm
                level = 1
            elif result > 7 and result < 13:        # -99dBm -> -89dBm
                level = 2
            elif result > 12 and result < 18:       # -89dBm -> -79dBm
                level = 3
            elif result > 17 and result < 23:       # -79dBm -> -69dBm
                level = 4
            elif result > 22 and result < 28:       # -69dBm -> -59dBm
                level = 5
            elif result > 27 and result < 31:       # -59dBm -> -53dBm
                level = 6
            elif result >= 31:                      # >-53dBm
                level = 7
            return level


    def getnoSMS(self):
        return self.noSMS


    def gettotSMS(self):
        return self.totSMS


    def getSMS(self, SMSindex):
        global readStringLen
        SMSmessage = ["", "", "", ""]

        try:
            serialCOM.connection.write("AT+CMGR=" + str(SMSindex) + "\r")
        except:
            raise GsmModuleWriteError
        try:
            serialCOM.retrieveData(["OK", "ERROR"], 5)
        except:
            raise GsmModuleReadError
        else:
            dataBuffer = serialCOM.dataBuffer

            processed=dataBuffer.split(",",4)
            buffer = processed[0].split("\"")
            SMSmessage[0] = replace(str(buffer[1]), "\"", "")           # type(REC) READ/UNREAD
            SMSmessage[1] = replace(str(processed[1]), "\"", "")        # sender number
            buffer = processed[3].split("\"")                           # this is junk
            buffer = processed[3].split("\"")
            SMSmessage[2] = buffer[1]                                   # date and time
            SMSmessage[3] = str(buffer[2])[2:buffer[2].find('OK')]      # message, payload

            #Logger.debug(SMSmessage[0]);
            #Logger.debug(SMSmessage[1]);
            #Logger.debug(SMSmessage[2]);
            #Logger.debug(SMSmessage[3]);
            return SMSmessage


    def sendSMS(self, phoneNumber, phoneType, message):
        res = 0
        try:
            serialCOM.connection.write("AT+CMGS=\"" + phoneNumber + "\"," + phoneType + "\r")
        except:
            raise GsmModuleWriteError
        try:
            serialCOM.retrieveData([">", "ERROR"],5)
        except:
            raise GsmModuleReadError
        message = message + chr(0x1A)                               # adding "send message char" to the message
        try:
            serialCOM.sendATCommand(message, ["OK", "ERROR"], 30)
        except:
            raise SendSMSError


        else:           #de scos ASAP >>>>>>
            return 0    #de scos ASAP >>>>>>


    def refreshSMSno(self):
        errCounter = 0
        while True:
            serialCOM.clearInput()
            try:
                serialCOM.sendATCommand("AT+CPMS?", ["OK", "+CMS ERROR:"], 10)
            except:
                if errCounter < 5:
                    errCounter = errCounter + 1
                else:
                    raise SMSNumberQuerryError
            else:
                break
            sleep(0.5)

        dataBuffer = serialCOM.dataBuffer
        try:
            processed = dataBuffer.split(",")
            self.noSMS = int(processed[1])
            self.totSMS = int(processed[2])
        except:
            raise SMSNumberQuerryError


        else:               #de scos ASAP >>>
            return 0        #de scos ASAP >>>


    def deleteSMS(self, SMSindex):
        errCounter = 0
        while True:
            serialCOM.clearInput()
            try:
                serialCOM.sendATCommand("AT+CMGD=" + str(SMSindex), ["OK", "+CMS ERROR:"],10)
            except:
                if errCounter < 5:
                    errCounter = errCounter + 1
                else:
                    raise DeleteSMSError
            else:
                break
            sleep(0.5)

        return 0        #de scos ASAP >>>


    def deleteMulSMS(self, category):
        errCounter = 0
        while True:
            serialCOM.clearInput()
            try:
                #sendATCommand("AT+QMGDA=\"DEL " + "" + category + "\"", ["OK", "+CMS ERROR:"], 10)
                serialCOM.sendATCommand("AT+CMGD=1,4", ["OK", "+CMS ERROR:"], 10)
            except:
                if errCounter < 5:
                    errCounter = errCounter + 1
                else:
                    raise DeleteMultipleSMSError
            else:
                break
            sleep(0.5)

        return 0    #de scos ASAP >>>


#Class objects
gsmModem = GSMModem()
