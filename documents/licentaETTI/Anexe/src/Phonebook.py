from globalPara import *

import json

class Phonebook:
    people = []
    groups = []

    def __init__(self):
        self.people = []
        self.groups = []
        if self.setup() is True:
            Logger.error('Error while opening/reading phonebook.json')


    def setup(self):
        try:

            with open(phonebook_path + 'phonebook.json') as data_file:
                data = json.load(data_file)

            self.people = data['employees']
            self.groups = data['groups'].keys()

            for employee in self.people:
                employee.update({'Group':[]})
                for group in data['groups']:
                    if int(employee['ID']) in data['groups'][group]:
                        employee['Group'].append(group)

        except:
            return True


        return None

    def getGroups(self):
        return self.groups

    def getGroupConts(self, group, option):
        numbers = []
        emails = []
        if group in self.groups:
            for employee in self.people:
                if group in employee['Group']:
                    numbers.append(employee['PhoneNo'])
                    emails.append(employee['Email'])
            if option == 'PhoneNo':
                return numbers
            else:
                return emails
        else:
            Logger.warning('Group %s not found!' % group)
            return None

    def getName(self, phoneno):
        for employee in self.people:
            if employee['PhoneNo'] == phoneno:
                return employee['Name']
        return None

    def checkPhoneNo(self, phoneno):
        for employee in self.people:
            if employee['PhoneNo'] == phoneno:
                return True
        return False

    def checkEmail(self, email):
        for employee in self.people:
            if employee['Email'] == email:
                return True
        return False


#Class objects
phonebook = Phonebook()
