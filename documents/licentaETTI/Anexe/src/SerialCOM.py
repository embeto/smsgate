from globalPara import *
from Errors import *

import time
import serial
import sys
import traceback

class SerialCOM:
    def __init__(self):
        pass


    def startConnection(self):
        try:
            self.connection = serial.Serial("/dev/ttyAMA0", serialSpeed, timeout=1)
        except:
            self.stopConnection()
            raise SerialStartError
        else:
            try:
                self.connection.write("+++\r\n")
                self.connection.write(chr(0x1B) + "\r\n")
                self.connection.write("AT\r\n")
                self.connection.write("ATE1\r\n")
                self.connection.write("ATV1\r\n")
                self.clearSInput()
                self.clearInput()
            except:
                raise SerialSetupError

            self.clearSInput()
            self.clearInput()


    def stopConnection(self):
        try:
            self.connection.close()
        except:
            raise SerialStopError


    def retrieveData(self, endChars, timeOut):
        self.dataBuffer = ''
        timestamp = time.time()
        while True:
            if time.time() - timestamp > timeOut:
                break
            try:
                self.dataBuffer = self.dataBuffer + self.connection.read()
            except:
                raise SerialRecError
            if self.dataBuffer:
                for endChar in endChars:
                    if endChar in self.dataBuffer:
                        self.connection.flushInput()
                        return
        self.connection.flushInput()
        raise SerialRecError


    def sendATCommand(self, command, endChars, timeOut):
        for i in range(ATcmdNOFRetry):
            try:
                self.connection.write(command + "\r\n")
            except:
                raise SerialCommWriteError
            try:
                self.retrieveData(endChars, timeOut)
            except:
                raise SerialCommReadError
            else:
                return
        raise SerialCommFatalError


    def clearInput(self):
        self.connection.flushInput()


    def clearSInput(self):
        if (self.connection.inWaiting()):
            self.connection.read(4096)


    def readLine(self, timeout):
        self.dataBuffer = ''
        timestamp = time.time()
        while (True):
            char = self.connection.read()
            if char:
                if ord(char) == 0x0A:        # if char is line feed
                    break
                self.dataBuffer = self.dataBuffer + char
            if (timeout > 0 and time.time() - timestamp > timeout):
                raise SerialReadLineError


serialCOM = SerialCOM()
