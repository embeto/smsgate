class SerialStartError(Exception):
    def __init__(self):
        pass

class SerialStopError(Exception):
    def __init__(self):
        pass

class SerialSetupError(Exception):
    def __init__(self):
        pass

class SerialRecError(Exception):
    def __init__(self):
        pass

class SerialCommWriteError(Exception):
    def __init__(self):
        pass

class SerialCommReadError(Exception):
    def __init__(self):
        pass

class SerialCommFatalError(Exception):
    def __init__(self):
        pass

class SerialReadLineError(Exception):
    def __init__(self):
        pass


# Errors thrown by GSMModem object
class GPIOinputError(Exception):
    def __init__(self):
        pass

class GPIOoutputError(Exception):
    def __init__(self):
        pass

class ModemAlreadyOnError(Exception):
    def __init__(self):
        pass

class ModemAlreadyOffError(Exception):
    def __init__(self):
        pass

class ModemPowerOnError(Exception):
    def __init__(self):
        pass

class ModemPowerOffError(Exception):
    def __init__(self):
        pass

class RequestedPinError(Exception):
    def __init__(self):
        pass

class GSMModuleSetupError(Exception):
    def __init__(self):
        pass

class IMEIQuerryError(Exception):
    def __init__(self):
        pass

class IMSIQuerryError(Exception):
    def __init__(self):
        pass

class SMSNumberQuerryError(Exception):
    def __init__(self):
        pass

class SignalLevelQuerryError(Exception):
    def __init__(self):
        pass

class NwRegistrationError(Exception):
    def __init__(self):
        pass

class GsmModuleWriteError(Exception):
    def __init__(self):
        pass

class GsmModuleReadError(Exception):
    def __init__(self):
        pass

class SendSMSError(Exception):
    def __init__(self):
        pass

class DeleteSMSError(Exception):
    def __init__(self):
        pass

class DeleteMultipleSMSError(Exception):
    def __init__(self):
        pass


# Errors thrown by SysManager object
class GPIOSetupError(Exception):
    def __init__(self):
        pass

class GPIOReleaseError(Exception):
    def __init__(self):
        pass
