# !/usr/bin/python
from Daemon import *


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            if daemon.getPID():
                print 'smsgate daemon is already running!Use stop before start'
                sys.exit(1)
            else:
                daemon.start()
        elif 'stop' == sys.argv[1]:
            if not daemon.getPID():
                print 'NO smsgate daemon is running!Use start before stop'
                sys.exit(1)
            else:
                daemon.stop()
        elif 'status' == sys.argv[1]:
            if not daemon.getPID():
                print 'NO smsgate daemon is running!Use start before status'
                sys.exit(1)
            else:
                os.kill(daemon.getPID(), signal.SIGUSR1)
                time.sleep(10)

        elif 'help' == sys.argv[1]:
            print "usage: %s start|stop|restart|status|help" % sys.argv[0]
        else:
            print "Unknown command. See help for more info"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart|status|help" % sys.argv[0]
        sys.exit(2)
