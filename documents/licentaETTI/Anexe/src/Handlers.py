from globalPara import *
from Phonebook import *
from Email import *
from GSMModem import *

import subprocess


class Handlers:
    def __init__(self):
        pass

    def validMailAddr(self, address):
        address = address.strip()
        if address.find('@') == -1:
            return False
        address = address.split('@')
        if len(address) != 2:
            return False
        if not address[1] in allowed_domains:
            return False
        for special_char in spec_EMLchars:
            if address[0].find(special_char) != -1:
                return False
        return True

    def validPhoneNo(self, phoneno):

        phoneno = phoneno.strip()
        if len(phoneno) != 10:
            return False
        if not phoneno.isdigit():
            return False
        return True

    def validFileName(self, filename):
        for special_char in spec_FSchars:
            if filename.find(special_char) != -1:
                return False
        return True

    def handleSMS(self, sender_no, sms):
        checked_email = []
        sender_no = sender_no.strip()
        if sender_no[0] == '+':
            sender_no = sender_no[2:]

        if phonebook.checkPhoneNo(sender_no):
            raw_sms = sms
            sms = sms.split('#')

            if sms[0].strip().lower() == 'eml':
                if len(sms) < 4:  # must be at least 4 : <command>#<sender_name>#<Subject>#<Body>
                    Logger.warning('Wrong eml action SMS format from %s!' % sender_no)
                    return
                sender_name = phonebook.getName(sender_no)
                for recipient in sms[1:len(sms) - 2]:  # iterate in recipients attributes of the SMS
                    recipient = recipient.strip().lower()
                    if recipient in phonebook.getGroups():  # if recipient is a group
                        Logger.info('Sending email to all the people in %s group' %recipient)
                        to_list = phonebook.getGroupConts(recipient, 'Email')
                        for toMail in to_list:
                            if not toMail in checked_email:
                                email.lock.acquire()
                                if email.testSMTPping():
                                    try:
                                        if email.sendMail(sender_name, toMail, sms[len(sms) - 2].strip(),
                                                               sms[len(sms) - 1]) == -1:
                                            if email.lock.locked():
                                                email.lock.release()
                                            return -1
                                    finally:
                                        if email.lock.locked():
                                            email.lock.release()
                                    checked_email.append(toMail)
                                else:
                                    Logger.error('Server in unreachable! watchdog will restart ppp connection in a moment...')
                                    if email.lock.locked():
                                        email.lock.release()
                                    return -1

                    elif self.validMailAddr(recipient):  # if recipient is a valid Mail address
                        if not recipient in checked_email:
                            email.lock.acquire()
                            if email.testSMTPping():
                                try:
                                    if email.sendMail(sender_name, recipient, sms[len(sms) - 2].strip(),
                                                           sms[len(sms) - 1]):
                                        if email.lock.locked():
                                            email.lock.release()
                                        return -1
                                finally:
                                    if email.lock.locked():
                                        email.lock.release()
                                checked_email.append(recipient)
                            else:
                                Logger.error('Server in unreachable! watchdog will restart ppp connection in a moment...')
                                if email.lock.locked():
                                    email.lock.release()

                    else:
                        Logger.warning('%s is not a valid E-mail address or gropup name!' % recipient)
                return 1


            elif sms[0].strip().lower() == 'cmd':
                if len(sms) < 2:  # must be at least 2 : <command>#<script1>
                    Logger.warning('Wrong cmd action SMS format from %s!' % sender_no)
                    return
                for script in sms[1:]:
                    script = script.strip()
                    process = subprocess.Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                    result = process.communicate('find ' + commands_path + ' -name ' + script)
                    if result[0]:
                        Logger.info('Command %s was found and will be executed now' % script)
                        process = subprocess.Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                        result = process.communicate(result[0])
                        Logger.info('Command %s executed. Result is: %s' %(script, result))
                    else:
                        Logger.warning('Command %s was NOT found!' % script)
                return 1

            elif sms[0].strip().lower() == 's2f':
                if len(sms) != 3:  # must be exactly 3 : <command>#<output_file_name>#<text>
                    Logger.warning('Wrong s2f action SMS format from %s!' % sender_no)
                    return
                file_name = sms[1].strip()
                if self.validFileName(file_name):
                    body = sms[2]
                    command1 = 'touch ' + txt_out_path
                    command2 = 'echo \"' + body + "\" > " + txt_out_path + file_name
                    process = subprocess.Popen("{}; {}".format(command1, command2), stdin=subprocess.PIPE,
                                               stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,
                                               close_fds=True)
                    result, error = process.communicate()
                    if result or error:
                        Logger.warning('Error creating new file in %s folder' % txt_out_path)
                    else:
                        Logger.info('Succes creating %s file!' % file_name)
                        return 1
                else:
                    Logger.warning('%s is not a valid file name!' % file_name)

            elif sms[0].strip().lower() == 'ord':
                    if sms[1].strip().lower() == 'getsts':
                        #TODO send to sender an sms with the status ('Light') of the system
                        pass
                    elif sms[1].strip().lower() == 's2fsts':
                        #TODO save to file current status of the system
                        pass
                    elif sms[1].strip().lower() == 's2flog':
                        #TODO save to file logging of current day of the system
                        pass
                    else:
                        Logger.warning('Unknown ord From: %s Content: %s' % (sender_no, raw_sms))
			return 1
            else:
                Logger.warning('Unknown SMS content From: %s Content: %s' % (sender_no, raw_sms))
		return 1

        else:
            Logger.warning('Unauthorized SMS message From: %s Content: %s' % (sender_no, sms))

    def handleEmail(self, email):
        checked_numbers = []
        subject = email['Subject'].split('#')
        if subject[-1].strip().upper() != email_key or len(subject) < 3:
            Logger.warning('Wrong subject format for email. From: %s Subject: %s Body: %s' % (
                email['From'], email['Subject'], email['Body']))
        else:
            if subject[0].strip().lower() == 'sms':
                for recipient in subject[1:- 1]:  # iterate in recipients attributes of the mail
                    recipient = recipient.strip().lower()
                    if recipient in phonebook.getGroups():  # if recipient is a group
                        for toNum in phonebook.getGroupConts(recipient, 'PhoneNo'):
                            if not toNum in checked_numbers:
                                Logger.info("Sending '%s' SMS to %s in group %s" % (email['Body'][:-1], toNum, recipient))
                                gsmModem.lock.acquire()
                                try:
                                    if gsmModem.sendSMS(toNum, '129', email['Body']) == 1:
                                        return -1
                                finally:
                                    if gsmModem.lock.locked():
                                        gsmModem.lock.release()
                                checked_numbers.append(toNum)
                    elif self.validPhoneNo(recipient):  # if recipient is a valid PhoneNo
                        if not recipient in checked_numbers:
                            Logger.info("Sending '%s' SMS to %s " % (email['Body'][:-1], recipient))
                            gsmModem.lock.acquire()
                            try:
                                if gsmModem.sendSMS(recipient, '129', email['Body']) == 1:
                                    return -1
                            finally:
                                if gsmModem.lock.locked():
                                    gsmModem.lock.release()
                            checked_numbers.append(recipient)
                    else:
                        Logger.warning('%s is not a valid phone number or group!' % recipient)
                return 1

            elif subject[0].strip().lower() == 'cmd':
                if len(subject) < 3:  # must be at least 3 : cmd#<script1>#email_key
                    Logger.warning('Wrong cmd action Email format from %s!' % email['From'])
                    return
                for script in subject[1:-1]:
                    script = script.strip()
                    process = subprocess.Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                    result = process.communicate('find ' + commands_path + ' -name ' + script)
                    if result[0]:
                        Logger.info('Command %s was found and will be executed now' % script)
                        process = subprocess.Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                        result = process.communicate(result[0])
                        Logger.info('Command %s executed. Result is: %s' %(script, result))
                    else:
                        Logger.warning('Command %s was NOT found!' % script)
                return 1

            elif subject[0].strip().lower() == 's2f':
                if len(subject) != 3:  # must be exactly 3 : <command>#<output_file_name>#email_key
                    Logger.warning('Wrong s2f action Email format from %s!' % email['From'])
                    return
                file_name = subject[1].strip()
                if self.validFileName(file_name):
                    body = email['Body']
                    command1 = 'touch ' + txt_out_path
                    command2 = 'echo \"' + body + "\" > " + txt_out_path + file_name
                    process = subprocess.Popen("{}; {}".format(command1, command2), stdin=subprocess.PIPE,
                                               stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,
                                               close_fds=True)
                    result, error = process.communicate()
                    if result or error:
                        Logger.warning('Error creating new file in %s folder' % txt_out_path)
                    else:
                        Logger.info('Succes creating %s file!' % file_name)
                        return 1
                else:
                    Logger.warning('%s is not a valid file name!' % file_name)

            elif subject[0].strip().lower() == 'ord':
                if subject[1].strip().lower() == 'getsts':
                    #TODO send to sender an email with the status ('Heavy') of the system
                    pass
                elif subject[1].strip().lower() == 's2fsts':
                    #TODO save to file current status of the system
                    pass
                elif subject[1].strip().lower() == 's2flog':
                    #TODO save to file logging of current day of the system
                    pass
                else:
                    Logger.warning('Unknown ord From: %s Subject: %s' % (email['From'], email['Subject']))
            else:
                Logger.warning('Unknown Email content From: %s Content: %s Body: %s' % (email['From'], email['Subject'], email['Body']))

    def handleFS(self, path):
        checked_emails = []
        checked_numbers = []
        if '.sms' in path:
            try:
                f = open(path, 'r')
                raw_file = f.read()
            except:
                Logger.error('Unable to open/read file/from file %s' %path)
                return -1
            finally:
                if raw_file.find('To:') == -1 or raw_file.find('Body:') == -1:
                    Logger.error('%s file has no corresponding tags [To:,Body:]!' % path)
                else:
                    recipients = raw_file[3:raw_file.find('Body:')]
                    recipients = recipients.replace('\n', '').strip()
                    body = raw_file[raw_file.find('Body:') + 5:]
                    body = body.strip()
                    if len(body) > default_SMSsize:
                        Logger.warning('File %s has Body too large, only %s chars will be considered' %(path, default_SMSsize))
                        body = body[0:default_SMSsize]
                    recipients = recipients.split('#')
                    for recipient in recipients:  # iterate in recipients attributes of file
                        recipient = recipient.strip().lower()
                        if recipient in phonebook.getGroups():  # if recipient is a group
                            to_list = phonebook.getGroupConts(recipient, 'PhoneNo')
                            for toNum in to_list:
                                if not toNum in checked_numbers:
                                    Logger.info("Sending '%s' SMS to %s in group %s" % (body, toNum, recipient))
                                    gsmModem.lock.acquire()
                                    try:
                                        if gsmModem.sendSMS(toNum, '129', body) == 1:
                                            if gsmModem.lock.locked():
                                                gsmModem.lock.release()
                                            return -1
                                    except:
                                        return -1
                                    finally:
                                        if gsmModem.lock.locked():
                                            gsmModem.lock.release()
                                    checked_numbers.append(toNum)
                        elif phonebook.checkPhoneNo(recipient):  # if recipient is a valid PhoneNo
                            if not recipient in checked_numbers:
                                Logger.info("Sending '%s' SMS to %s " % (body, recipient))
                                gsmModem.lock.acquire()
                                try:
                                    if gsmModem.sendSMS(recipient, '129', body) == 1:
                                        if gsmModem.lock.locked():
                                            gsmModem.lock.release()
                                        return -1
                                except:
                                    return -1
                                finally:
                                    if gsmModem.lock.locked():
                                        gsmModem.lock.release()
                                checked_numbers.append(recipient)
                        elif self.validPhoneNo(recipient):
                            Logger.warning('Could not send SMS to unauthorized phone number %s' % recipient)
                        else:
                            Logger.warning('%s is not a valid phone number or gropup name!' % recipient)
                    return 1

        elif '.eml' in path:
            try:
                f = open(path, 'r')
                raw_file = f.read()
            except:
                Logger.error('Unable to open/read file/from file %s' %path)
                return -1
            finally:
                if raw_file.find('To:') == -1 or raw_file.find('From:') == -1 or raw_file.find('Subject:') == -1 or raw_file.find('Body:') == -1:
                    Logger.error('%s file has no corresponding tags [To:,From:,Subject:,Body:]!' % path)
                else:
                    recipients = raw_file[3:raw_file.find('From:')]
                    recipients = recipients.replace('\n', '').strip()
                    sender_name = raw_file[raw_file.find('From:') + 5:raw_file.find('Subject:')]
                    sender_name = sender_name.replace('\n', '').strip()
                    subject = raw_file[raw_file.find('Subject:') + 8:raw_file.find('Body:')]
                    subject = subject.replace('\n', '').strip()
                    body = raw_file[raw_file.find('Body:') + 5:].strip()
                    recipients = recipients.split('#')
                    for recipient in recipients:  # iterate in recipients attributes of file
                        recipient = recipient.strip().lower()
                        if recipient in phonebook.getGroups():  # if recipient is a group
                            Logger.info('Sending email to all the people in %s group' %recipient)
                            to_list = phonebook.getGroupCont(recipient, 'Email')
                            for toMail in to_list:
                                if not toMail in checked_emails:
                                    email.lock.acquire()
                                    if email.testSMTPping():
                                        try:
                                            if email.sendMail(sender_name, toMail, subject, body) == -1:
                                                if email.lock.locked():
                                                    email.lock.release()
                                                return -1
                                        except:
                                            return -1
                                        finally:
                                            if email.lock.locked():
                                                email.lock.release()
                                        checked_emails.append(toMail)
                                    else:
                                        Logger.error('Server in unreachable! watchdog will restart ppp connection in a moment...')
                                        if email.lock.locked():
                                            email.lock.release()

                        elif phonebook.checkEmail(recipient):  # if recipient is a valid Email address
                            if not recipient in checked_emails:
                                email.lock.acquire()
                                if email.testSMTPping():
                                    try:
                                        if email.sendMail(sender_name, recipient, subject, body) == -1:
                                            if email.lock.locked():
                                                email.lock.release()
                                            return -1
                                    except:
                                        return -1
                                    finally:
                                        if email.lock.locked():
                                            email.lock.release()
                                    checked_emails.append(recipient)
                                else:
                                    Logger.error('Server in unreachable! watchdog will restart ppp connection in a moment...')
                                    if email.lock.locked():
                                        email.lock.release()

                        elif self.validMailAddr(recipient):
                            Logger.warning('Could not send email to unauthorized email address %s' % recipient)
                        else:
                            Logger.warning('%s is not a valid E-mail address or gropup name!' % recipient)
                    return 1

        else:
            Logger.error("%s has a wrong FS extension! Only *.sms and *.mail are allowed!" % path)


#Class objects
handlers = Handlers()
