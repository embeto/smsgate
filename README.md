﻿# SMSGate - LAN to GSM bridge #
* https://embeto.com/smsgate/
* Developed under the AMIQ Education Program, an AMIQ Blog post avaliable [here](https://www.amiq.com/consulting/2017/03/24/mentoring-young-talent-through-hands-on-applications/)
* Bridge between LAN and GSM using d-uGSM module 
* The system is designed to operate as a slave to a master server and send SMS/Email alers about certain events: power loss, Internet connector lost
* The system has a GSM connection including data (access to email) 
* based on d-uGSM module (with Quectel UG95 IC) and Raspberry Pi 3
* Author: Adrian-Razvan Petre @Embeto Electronics

## Application structure: ##
* The main app has three main flows : SMS, FS (file system) and POP(e-mail service).
* The main app will treat a command coming from one flow and redirect it to another one.
* In the old implementation, the one described in this [AMIQ blog post](https://www.amiq.com/consulting/2017/03/24/mentoring-young-talent-through-hands-on-applications/), an Ethternet connection was required (no longer needed in the final one)
* In the last implementeation available, the internet connection comes from a PPP connetion from GPRS via the d-uGSM module

## Repository structure: ##
* documents 
	* c-uGSM_module - GSM docs, official http://itbrainpower.net/ sketch examples and the initial API provided by DragosIosub
	* datasheets - datasheets for all major components used in the PCB implementation
	* documentation - SMSGate User's Guide and Article (both docx and pdf files)
	* licentaETTI - Bachelor's thesis document for Faculty of Electronics by Adrian-Razvan Petre about SMSGate. Written in LaTeX in romanian
	* schematics - each tapeout version of powerModule and commandModule is listed here in a directory containing the corresponding BOM and PDF schematic. There are also block schematics and large BOM for the whole system
	
* pcbDesign - cadence Capture and Allegro schematic design and documents
 	* commandModule - commandModule is the LEDs and RR push button Printed Circuit Board mounted on the front of the SMSGate device
		* schematics - Working directory for commandModule circuit board
		* tapeouts (see *circuit Board design* section for more)
	* powerModule
		* schematics - Working directory for powerModule
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files

* sources - executable code files for the SMSGate application 
	* ppp - PPP start and stop scripts
	* py - Python application source code
		* lib - Library files and definition of some methods from GSM_modem class
		* src - Python source files
		* test - unitest cases for some basic features of the implemented classes
		* tmp - scratchpad for python code snippets
	* sh - scrips used in the work in progress stage

* var_smsgate - this whole content must be copied in /var/smsgate at the first initialization of the SMSGate application on a brand new Rasbian OS.
	* template.eml and template.sms are two tempalte files for a new sms and email command 
	* If this type of files are pasted in the input directory (/var/smsgate/input) the SMSGate daemon will parse them immediately and delete them after execution.
		 
## Circuit Board design ##
* commandModule 
	* v1.2 - (factory produced) - Project version with 3D models for components. In some parts (PCB design working directory) called v1.3.
* powerModule
	* v1.0 - (in house produced) -	PCB breakout designed only for LTC4150 typical application	
	* v1.1 - (in house produced) -	first implmentation using relay switching logic (huge design flaw)
	* v1.2 - (factory produced) 
		* design flaw: LTC 4150 power pin connected to 3V3 and it should have been linked to battery voltage (resolved with in house small link under MSOP package)
		* design flaw: battery charging circuit had one Schottky diode which caused high power dissipation on regulator transistor. Replaced with two regular diodes to take the voltage drop
		* desgin flaw: C4 and C6 were no loger needed and are not populated on board   
		* GSM module support for c-uGSM (not having 2 communcation interfaces and which was dropped)
		* mounting holes in random positions and only 2 
	* v1.3 - (dropped) - remake of v1.2 with a few extra features:
		* I2C connectors are externalized from the IDC10 connector to a CON2 for later use
		* 4 mounting holes positioned in the 4 corners of the board
		* GSM module support for d-uGSM (with 2 interfaces allowing both GSM and GPRS usage) 	
		* d-uGSM module unused pins externalised on a connector for later use 

## Raspbian configuraton: ##
* Install Raspbian and mount its image
* Disable Raspberry Serial Console
* sudo raspi-config -> 8. Advanced Options -> A8. Serial -> Disable 
* Enable UART
* sudo vim /boot/config.txt and update enable_uart=0 to enable_uart=1 
* sudo apt-get install python3.
* edit **$HOME/.bashrc** and add **export PYTHONPATH=$PYTHONPATH:$HOME/smsgate/src/py/lib:$HOME/smsgate/src/py/src**
* login as root and add to crontab (**crontab -e**) the line **`@reboot` rm -rf /var/smsgate/smsgate.pid** to remove smsgate.pid if somehow is still present at a system startup (in case of an unexpected shutdown)

## Documentation links
* [itbrainpower original GSM project](http://itbrainpower.net/micro-GSM-shield-module-cuGSM/GSM-micro-shield-board-module-RaspberryPI-Arduino-c-uGSM-features-code-examples)
* [release UART interface at RPi3](https://learn.adafruit.com/adafruit-nfc-rfid-on-raspberry-pi/freeing-uart-on-the-pi)
* [Logging in python](https://docs.python.org/3/library/logging.html#levels)
* [About IMAP and SMTP](https://automatetheboringstuff.com/chapter16/)
* [Unitest in python](https://docs.python.org/2/library/unittest.html)
* [unix daemon in Python](http://web.archive.org/web/20131025230048/http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/)

## Fusion 360 system Housing 
* view/download the initial enclosure design in Web Browser [here](https://a360.co/3pGM7rL)
* view/download the final enclosure design in Web Browser [here](http://a360.co/2la1SIm)

## Acknowledgements
* Dragos  Iosub from ITBrainPower provided a library sketch  with the d-uGSM Module. The communication with the module in the SMSGate application is based on this API, which was a very good starting point.

